﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Kuroneko {

    public class UISystem : MonoBehaviour {

        public UIComponent StartScreen;

        public UnityEvent onSwitchScreen = new UnityEvent();

        public Component[] screens = new Component[0];

        private UIComponent previousScreen;
        public UIComponent PreviousScreen { get { return currentScreen; } }
        private UIComponent currentScreen;
        public UIComponent CurrentScreen { get { return currentScreen; } }

        public Image Fade;
        public float FadeInTime = 1f;
        public float FadeOutTime = 1f;

        void Start() {
            screens = GetComponentsInChildren<UIComponent>(true);

            //InitalizeScreen();

            if (StartScreen) {
                SwitchScreen(StartScreen);
            }

           // if (Fade) {
           //     Fade.gameObject.SetActive(true);
           // }

           // FadeIn();
        }

        void Update() {

        }

        public void SwitchScreen(UIComponent NextScreen){

            if (NextScreen) {
                if (currentScreen){
                    currentScreen.CloseScreen();
                    previousScreen = currentScreen;

                }
                currentScreen = NextScreen;
                currentScreen.gameObject.SetActive(true);
            }

            if (onSwitchScreen != null) {
                onSwitchScreen.Invoke();
            }
        }
    
        public void GoToPreviousScreen() {
            if (previousScreen) {
                SwitchScreen(previousScreen);
            }
        }

        public void LoadingScreen(int sceneIndex) {
            SceneManager.LoadScene(sceneBuildIndex:1);
           // StartCoroutine(WaitToLoadScene(sceneIndex));
        }

        public void FadeIn() {
            if (Fade) {
                Fade.CrossFadeAlpha(0f, FadeInTime, false);
            }
        }

        public void FadeOut() {
            if (Fade) {
                Fade.CrossFadeAlpha(1f, FadeOutTime, false);
            }
        }

        IEnumerator WaitToLoadScene(int sceneIndex) {
            yield return null;
        }

        void InitalizeScreen() {
            foreach (var screen in screens) {
                screen.gameObject.SetActive(true);
            }
        }
    }
}