﻿using UnityEngine;

public class ExampleMoveScript : MonoBehaviour
{
    // Move object using accelerometer
    float speed = 5.0f;

    public float sensitivity, rotateSpeed;

    protected Joystick joystick;

    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
    }

    void Update()
    {
        Vector3 dir = Vector3.zero;

        // we assume that device is held parallel to the ground
    
        // remap device acceleration axis to game coordinates:
        // XY plane of the device is mapped onto XZ plane
        dir.x = Input.acceleration.x;
        dir.z = Input.acceleration.y;

        // clamp acceleration vector to unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        // Make it move 10 meters per second instead of 10 meters per frame...
        dir *= Time.deltaTime;

        transform.Rotate(0f, joystick.Horizontal * rotateSpeed, 0f);

        // Move object
        if ((dir.x > sensitivity || dir.x < -sensitivity) || (dir.z > sensitivity || dir.z < -sensitivity))
            transform.Translate(dir * speed);
    }
}