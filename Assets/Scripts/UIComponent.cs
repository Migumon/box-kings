﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Kuroneko {

    public class UIComponent : MonoBehaviour {

        public UnityEvent onScreenStart = new UnityEvent();
        public UnityEvent onScreenClose = new UnityEvent();

        // Start is called before the first frame update
        void Start() {

        }

        //can play animations or add visuals
        public virtual void StartScreen() {
            if (onScreenStart != null) {
                onScreenStart.Invoke();
            }

            // Add animations if per start something happs
        }

        public virtual void CloseScreen() {
            if (onScreenClose != null) {
                onScreenClose.Invoke();
            }
        }
    }
}