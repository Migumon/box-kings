﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Class : ScriptableObject
{
    [Header("Player Stats")]
    public int health;
    public int armor;
    public int damage;

    [Header("Player Speed")]
    public float moveSpeed;
    public float rotateSpeed;
}