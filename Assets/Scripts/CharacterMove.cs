﻿using UnityEngine;

public class CharacterMove : MonoBehaviour
{
    public Class charClass;
    public GameObject playerBody;
    public float tiltSensitivity;

    protected Joystick joystick;

    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
    }

    void Update()
    {
        Vector3 dir = Vector3.zero;

        // we assume that device is held parallel to the ground
        // remap device acceleration axis to game coordinates:
        // XY plane of the device is mapped onto XZ plane
        dir.x = Input.acceleration.x;
        dir.z = Input.acceleration.y;

        // clamp acceleration vector to unit sphere
        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        // Make it move 10 meters per second instead of 10 meters per frame...
        dir *= Time.deltaTime;
        //Rotate Player
        playerBody.transform.Rotate(0f, joystick.Horizontal * charClass.rotateSpeed, 0f);

        // Move Player
        if ((dir.x > tiltSensitivity || dir.x < -tiltSensitivity) || (dir.z > tiltSensitivity || dir.z < -tiltSensitivity))
            transform.Translate(dir * charClass.moveSpeed);

    }
}