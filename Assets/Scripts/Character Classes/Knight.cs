﻿using UnityEngine;

[CreateAssetMenu(menuName = "Class/Knight")]
public class Knight : Class
{
    public float velocity;
}